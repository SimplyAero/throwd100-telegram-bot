#!/usr/bin/env python3
# -*- coding: utf-8 -*-

import re
import sys
import random

from telegram import (
    InlineQueryResultArticle,
    InputTextMessageContent,
    ParseMode
)
from telegram.ext import (
    Updater,
    InlineQueryHandler,
    CommandHandler
)


TOKEN = sys.argv[-1]
SOURCE_URL = "https://gitlab.com/SimplyAero/throwd100-telegram-bot"
THROW_REGEX = re.compile("^[1-9]\d*d[1-9]\d*([+-][1-9]\d*)?$")


def roll(query):
    query = query.lower()
    if not THROW_REGEX.fullmatch(query):
        raise IOError('Invalid query "{0}"'.format(query))
    if "+" in query:
        support = query.split("+")
        to_add = int(support[1])
        query = support[0]
    elif "-" in query:
        support = query.split("-")
        to_add = -int(support[1])
        query = support[0]
    else:
        to_add = 0
    splitted = query.split("d")
    throws = int(splitted[0])
    faces = int(splitted[1])
    # Checking if the number of throws and faces are lower than 100
    throws = 100 if throws > 100 else throws
    faces = 100 if faces > 100  else faces
    rolls = list()
    for _ in range(throws):
        # Generating the number
        rolls.append(random.randint(1, faces))
    # Extracting useful data for the user
    highest = max(rolls)
    lowest = min(rolls)
    total = sum(rolls) + to_add
    # Truncating the average at the second decimal place
    average = int((total / throws) * 100) / 100
    # Writing the result string
    result = "*Rolling {0} d{1}".format(throws, faces)
    if to_add > 0:
        result += "+{0}".format(to_add)
    elif to_add < 0:
        result += "-{0}".format(abs(to_add))
    result += "*\n"
    result += "*Results*\n" + "\n".join([str(value) for value in rolls])
    result += "\n*Max* {0}\n*Min* {1}\n*Total* {2}\n*Average* {3}"
    result = result.format(highest, lowest, total, average)
    return result


def start(bot, update):
    update.message.reply_text("Bot activated!\nType /help for instructions!")


def help(bot, update):
    """
    Message content:
        Standard:
        "/roll AdB" where A is the number of throws (max 100) and B the number
        of faces (max 100)
        Inline:
        "@rolld100bot AdB[+/-C]" where A is the number of throws (max 100),
        B the number of faces (max 100) and C (Optional) the number to add or
        subtract to the total
    """
    # Writing bot's instructions
    reply = '*Standard*\n"/roll AdB" where A is the number of throws (max 100)'
    reply += ' and B the number of faces (max 100)\n*Inline*\n"@rolld100bot Ad'
    reply += 'B" where A is the number of throws (max 100), B the number of'
    reply += ' faces (max 100) and C (Optional) the number to add or subtract '
    reply += 'to the total'
    update.message.reply_text(reply)


def roll_standard(bot, update):
    # Extracting the message for faster access
    message = update.message
    # Writing the result
    try:
        result = roll("".join(message.text.split(" ")[1:]))
    except IOError:
        result = "Invalid query\nType /help for instructions"
    message.reply_text(result, parse_mode=ParseMode.MARKDOWN)


def roll_inline(bot, update):
    # Extracting inline query for faster access
    inline_query = update.inline_query
    query = inline_query.query
    # Checking if the query is not empty
    if query:
        try:
            # Writing the result object
            result = InlineQueryResultArticle(
                id="throw",
                title="Throw",
                input_message_content=InputTextMessageContent(
                    roll(query),
                    parse_mode=ParseMode.MARKDOWN
                )
            )
        except IOError:
            # Writing the result object
            text = "Invalid query\nOpen @rolld100bot for instructions"
            result = InlineQueryResultArticle(
                id="Invalid",
                title="Invalid query",
                input_message_content=InputTextMessageContent(text)
            )
        bot.answer_inline_query(inline_query.id, [result], cache_time=0)
        


def source(bot, update):
    update.message.reply_text(SOURCE_URL)


def main():
    print("STARTING BOT...")
    # Creating bot
    updater = Updater(TOKEN)
    # Extracting the dispatcher for faster access
    dispatcher = updater.dispatcher
    # Defining handlers
    dispatcher.add_handler(CommandHandler("start", start))
    dispatcher.add_handler(CommandHandler("help", help))
    dispatcher.add_handler(CommandHandler("roll", roll_standard))
    dispatcher.add_handler(CommandHandler("source", source))
    dispatcher.add_handler(InlineQueryHandler(roll_inline))
    # Starting bot
    updater.start_polling()
    # Looping bot
    print("BOT RUNNING...")
    updater.idle()


if __name__ == '__main__':
    main()